# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-07-03 22:56+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../GnomeHello/main.py:37
msgid "Open..."
msgstr "Abrir..."

#: ../GnomeHello/main.py:46
msgid "All files"
msgstr "Todos os ficheiros"

#: ../GnomeHello/main.py:81
#, fuzzy, python-format
msgid "%i bytes; MIME type: %s"
msgstr "Tamanho: %i bytes; Tipo MIME: %s"

#: ../GnomeHello/main.py:83
#, fuzzy, python-format
msgid "%i bytes"
msgstr "Tamanho: %i bytes"

#: ../GnomeHello/main.py:85
#, python-format
msgid ""
"You have opened the file:\n"
"%s"
msgstr "Você abriu o ficheiro:\n%s"

#: ../pygnome-hello.desktop.in.h:1
msgid "PyGnomeHello"
msgstr "PyGnomeHello"

#: ../pygnome-hello.desktop.in.h:2
msgid "Sample PyGTK GNOME Application"
msgstr "Uma aplicação PyGTK GNOME de demonstração"

#: ../pygnome-hello.schemas.in.h:1
msgid "A Test GConf option"
msgstr "Uma opção de demonstração"

#: ../pygnome-hello.schemas.in.h:2
msgid "This demonstrates a GConf application preference."
msgstr "Isto demonstra uma preferência GConf de aplicação"

#: ../pygnome-hello.glade.h:1
msgid "<big>Welcome to <b>PyGnomeHello</b></big>"
msgstr "<big>Bem vindo a <b>PyGnomeHello</b></big>"

#: ../pygnome-hello.glade.h:2
msgid "File Size: "
msgstr "Tamanho do Ficheiro: "

#: ../pygnome-hello.glade.h:3
msgid "Some option: "
msgstr "Uma opção qualquer: "


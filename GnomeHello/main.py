import pygtk; pygtk.require('2.0')
import gtk
import gtk.glade
import gettext
import sys
import os
import os.path
import config

HAVE_GCONF = False
HAVE_VFS = False
try:
    import gnome
    assert gnome.gnome_python_version >= (2, 6, 0)

    try:
        import gconf
        HAVE_GCONF = True
    except:
        print >> sys.stderr, "gconf module not found"

    try:
        import gnomevfs
        HAVE_VFS = True
    except:
        print >> sys.stderr, "gnomevfs module not found"

except:
    print >> sys.stderr, "gnome-python >= 2.5.90 not found"
    HAVE_GCONF = False


gtk.glade.bindtextdomain(config.PACKAGE, os.path.join(config.datadir, "locale"))
gtk.glade.textdomain(config.PACKAGE)
gettext.install(config.PACKAGE, os.path.join(config.datadir, "locale"))


def get_file(parent):
    sel = gtk.FileChooserDialog(_("Open..."),
                                None,
                                gtk.FILE_CHOOSER_ACTION_OPEN,
                                (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                 gtk.STOCK_OPEN, gtk.RESPONSE_OK))
    sel.set_default_response(gtk.RESPONSE_OK)
    sel.set_local_only(not HAVE_VFS)

    filter = gtk.FileFilter()
    filter.set_name(_("All files"))
    filter.add_pattern("*")
    sel.add_filter(filter)

    sel.set_transient_for(parent)

    resp = sel.run()
    if resp != gtk.RESPONSE_OK:
        sel.destroy()
        sys.exit(0)
    try:
        if HAVE_VFS:
            return gnomevfs.URI(sel.get_uri())
        else:
            return sel.get_filename()
    finally:
        sel.destroy()

class Main(object):
    def __init__(self):
        xml = gtk.glade.XML(os.path.join(config.pkgdatadir, "pygnome-hello.glade"))
        xml.signal_autoconnect(self)
        
        if HAVE_GCONF:
            pref = gconf.client_get_default().get_string("/apps/pygnome-hello/test")
            if pref:
                xml.get_widget("pref_entry").set_text(pref)

        try:
            fname = sys.argv[1]
        except IndexError:
            fname = get_file(xml.get_widget("window1"))
        if HAVE_VFS:
            vfsinfo = gnomevfs.get_file_info(fname, (gnomevfs.FILE_INFO_GET_MIME_TYPE|
                                                     gnomevfs.FILE_INFO_FORCE_SLOW_MIME_TYPE))
            info = _("%i bytes; MIME type: %s") % (vfsinfo.size, vfsinfo.mime_type)
        else:
            info = _("%i bytes") % (os.stat(fname).st_size)
        xml.get_widget("file size entry").set_text(info)
        xml.get_widget("file name label").set_text(_("You have opened the file:\n%s") % fname)
        gtk.main()

    def on_pref_entry_changed(self, entry):
        if HAVE_GCONF:
            gconf.client_get_default().set_string("/apps/pygnome-hello/test", entry.get_text())

    def on_window1_destroy(self, window1):
        gtk.main_quit()

